The Commerce SecurePay.com.au module provides support for the SecurePay.com.au
payment gateway with the Drupal Commerce module.

Commerce SecurePay.com.au was written and is maintained by Stuart Clark
(deciphered) of Realityloop Pty Ltd.
- http://www.realityloop.com

Commerce SecurePay.com.au was initially developed for Business Spectator Pty Ltd
- http://www.businessspectator.com.au


Features
--------------------------------------------------------------------------------

* Integrates with:
  * Commerce Card on File - Adds the ability to setup and trigger
      SecurePay.com.au Periodic/Triggered payments.
  * Commerce Recurring Framework - Adds the ability to trigger recurring
      payments via the Commerce Card on File integration.


Required Modules
--------------------------------------------------------------------------------

* Drupal Commerce - http://drupal.org/project/commerce
